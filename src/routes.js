import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import Simulator from './components/simulator/Simulator';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Simulator} />
  </Route>
);