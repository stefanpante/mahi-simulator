/* eslint-disable import/default */
import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import './css/simulator.css';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import App from './components/App';
import '../node_modules/leaflet/dist/leaflet.css';

const store = configureStore();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);