import $ from "jquery";

class SimulationApi {
  static getSimulation(markers, time, callback) {
    const data = markers.map(marker => {
      return [marker.lat, marker.lng];
    });
    const departureTime = time.format('D/M/YYYY');
    $.ajax({
      type: "POST",
      url: '/coordinates',
      data: JSON.stringify({markers:data, departureTime: departureTime}),
      success: (data) => {
        return callback(data);
      },
      contentType: 'application/json'
    });
  }
}

export default SimulationApi;