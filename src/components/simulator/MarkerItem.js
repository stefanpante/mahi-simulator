import React from 'react';

const MarkerItem = ({marker, index, deleteMarker, stage}) => {
  function del() {
    deleteMarker(index);
  }

  return (
    <div className="marker-item" key={index}>
      <div className="text">Waypoint {index}</div>
      {stage == 1 && <div className="delete" onClick={del}><i className="fa fa-times" aria-hidden="true"></i></div>}
    </div>
  );
};

export default MarkerItem;