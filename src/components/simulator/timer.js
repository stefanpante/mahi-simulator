import React from 'react';

const Timer = ({time}) => {
  return (
    <div className="time">
      <div className="day">
        {time.format('DD/MM/YYYY') }
      </div>
      <div className="clock">
        {time.format('HH:MM') }
      </div>
    </div>
    );
};

export default Timer;