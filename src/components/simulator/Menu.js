import React from 'react';
import DatePicker from 'react-datepicker';
import MarkerList from './MarkerList';
import {connect} from 'react-redux';
import * as actions from '../../actions/markerActions';
import * as stageActions from '../../actions/stageActions';
import * as departureActions from '../../actions/departureActions';
import * as simulationActions from '../../actions/simulationActions';
import moment from 'moment';
import {bindActionCreators} from 'redux';

import 'react-datepicker/dist/react-datepicker.css';


class Menu extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.stageNames = ['Create Route', 'Finish Route', 'Simulate', 'Reset Simulator'];

    this.transitionStage = this.transitionStage.bind(this);
    this.deleteMarker = this.deleteMarker.bind(this);
    this.setDepartureTime = this.setDepartureTime.bind(this);
  }

  transitionStage() {

    if(this.props.stage === 2) {
      this.props.getSimulation(this.props.markers, moment(this.props.departureTime * 1000));
    }

    if(this.props.stage === 3){
      return this.props.resetStage();
    }
    this.props.transitionStage();
  }

  deleteMarker(index) {
    this.props.deleteMarker(index);
  }

  setDepartureTime(time) {
    time.hour(8);
    time.minutes(0);
    const timestamp = time.unix();
    this.props.setDepartureTime(timestamp);
  }

  render() {
    const buttonText = this.stageNames[this.props.stage];
    const disabled = !this.props.departureTime && this.props.stage === 2;
    const className = disabled ? 'disabled' : '';
    const departureTime = this.props.departureTime > 0 ? moment(this.props.departureTime * 1000) : false;
    const showDepartureTime = this.props.stage === 2 && departureTime > 0 || this.props.stage === 3;
    return (
      <div className="menu">
        <button onClick={this.transitionStage} className={className} disabled={disabled}> {buttonText} </button>
        {this.props.stage === 1 && <div className="explanation"> Click on the map to add waypoints</div>}
        {this.props.stage === 2 && <div className="explanation"> Select a departure date</div>}
        {showDepartureTime && <div className="explanation" style= {{color: 'black',fontWeight: 'bold'}}> {departureTime.format('DD/MM/YYYY hh:mm')}</div>}
        {this.props.stage === 2 && <div className="explanation"><DatePicker onChange={this.setDepartureTime} inline className="custom-datepicker" /></div>}
        <MarkerList markers={this.props.markers} deleteMarker={this.deleteMarker} stage={this.props.stage}/>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    markers: state.markers,
    stage: state.stage,
    departureTime: state.departureTime
  };
}

function mapDispatchToProps(dispatch) {
  const props = bindActionCreators(actions, dispatch);
  props.transitionStage = () => dispatch(stageActions.transitionStage());
  props.resetStage = () => dispatch(stageActions.resetStage());
  props.setDepartureTime = (time) => dispatch(departureActions.setDepartureTime(time));
  props.getSimulation= (markers, time) => dispatch(simulationActions.getSimulation(markers, time));
  return props;
}


export default connect(mapStateToProps, mapDispatchToProps)(Menu);