import React, {PropTypes} from 'react';
import RoutingMap from './Map';
import {connect} from 'react-redux';
import * as actions from '../../actions/markerActions';
import {bindActionCreators} from 'redux';

class Simulator extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    if(this.props.stage === 1){
      this.props.addMarker(event.latlng);
    }
  }

  render() {
    return (
      <RoutingMap onClick={this.onClick}
          markers={this.props.markers}
          simulation={this.props.simulation}
          departureTime={this.props.departureTime}/>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    markers: state.markers,
    stage: state.stage,
    simulation: state.simulation,
    departureTime: state.departureTime
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Simulator);