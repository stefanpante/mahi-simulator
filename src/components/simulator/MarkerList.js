import React from 'react';
import MarkerItem from './MarkerItem';
const MarkerList = ({markers, deleteMarker, stage}) => {
  if(markers.length === 0){
    return <div className="hidden"></div>;
  }

  const markerItems = markers.map((marker, index) => {
    return (<MarkerItem marker={marker} index={index} key={index} deleteMarker={deleteMarker} stage={stage}/>);
  });

  return (
    <div className="marker-list">
      {markerItems}
    </div>
  )
};

export default MarkerList;