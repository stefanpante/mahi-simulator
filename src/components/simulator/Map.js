import React from 'react';
import { Map, Marker, Popup, TileLayer, Polyline, latlng } from 'react-leaflet';
import moment from 'moment';
import Timer from './timer';
import 'leaflet';

const startIcon = L.divIcon({ className: 'start-icon' });
const endIcon = L.divIcon({
  className: 'end-icon',
  iconSize: L.point(35, 35)
});
const position = [50.407364, -0.535467];

const animationDuration = 20 * 1000; // 10 seconds;

class RoutingMap extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = { currentSimulationMarker: 0, simulating: false };
    this.currentSimulationMarker = 0;
    this.drawSimulation = this.drawSimulation.bind(this);

  }

  drawSimulation() {
    if (this.props.simulation.length === 0) {
      this.currentSimulationMarker = 0;
      return setTimeout(this.drawSimulation, 75);
    }

    if (this.state.currentSimulationMarker === this.props.simulation.length - 1) {
      this.setState({ simulating: false, currentSimulationMarker: 0 });
      return;
    }

    const timeout = Math.ceil(animationDuration / this.props.simulation.length);
    this.setState({ currentSimulationMarker: this.state.currentSimulationMarker + 1 });
    return setTimeout(this.drawSimulation, timeout);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.simulation.length > 0) {
      this.setState({ simulating: true, currentSimulationMarker: 0 });
      return this.drawSimulation();
    }

    this.setState({ simulating: false, currentSimulationMarker: 0 });
  }

  render() {
    const markersMap = this.props.markers.map((marker, index) => {
      return (
        <Marker position={marker} key={index}/>
      );
    });


    const markersSim = this.props.simulation.filter((marker) => marker[0] != null && marker[1] != null);
    const showBoat = this.state.simulating && this.props.simulation.length > 0;

    const time = moment(this.props.departureTime * 1000);
    time.add(30 * this.state.currentSimulationMarker, 'm');
    return (<div className="map-wrapper"><Map className ="map" center={position} zoom={5} dragging={false} zoomControl={false} onClick={this.props.onClick}>
      <TileLayer
        url='http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
      {markersMap}
      <Polyline positions={this.props.markers}/>
      <Polyline positions={markersSim} color="rgba(8, 78, 90, 0.7)"/>
      {showBoat && <Marker position={markersSim[this.state.currentSimulationMarker]} icon={endIcon}>
      </Marker>}
    </Map>
      {showBoat && <Timer time={time} />}
    </div>);
  }
}

export default RoutingMap;