import React, {PropTypes} from 'react';
import Simulator from './simulator/Simulator';
import Menu from './simulator/Menu';
import {connect} from 'react-redux';

import Loading from './common/Loading';
class App extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="container-fluid" >
        <Menu />
        <Simulator/>
        {this.props.loading && <Loading />}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    loading: state.loading
  };
}


export default connect(mapStateToProps)(App);