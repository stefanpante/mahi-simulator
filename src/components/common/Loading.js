import React from 'react';


const Loading = () => {

  return (
    <div className="loading">
      <div><i className="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
      <div className="loading-explanation"><span>Calculating the simulation.</span></div>
    </div>
  );

};

export default Loading;