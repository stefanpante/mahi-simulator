import * as types from './actionTypes';

export function transitionStage() {
  return { type: types.TRANSITION_STAGE };
}

export function resetStage() {
  return { type: types.RESET };
}