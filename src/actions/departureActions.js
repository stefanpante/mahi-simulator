import * as types from './actionTypes';

export function setDepartureTime(time) {
  return {type: types.SET_DEPARTURE_TIME, time: time};
}

export function resetDepartureTime() {
  return {type: types.RESET_DEPARTURE_TIME};
}