import * as types from './actionTypes';

export function startLoading() {
  return { type: types.START_LOAD };
}

export function loadFinished() {
  return { type: types.LOAD_FINISHED };
}