import * as types from './actionTypes';

export function addMarker(marker) {
  return {type: types.ADD_MARKER, marker: marker};
}

export function deleteMarker(markerIndex) {
  return {type: types.DELETE_MARKER, markerIndex: markerIndex};
}

export function resetMarkers() {
  return {type: types.RESET_MARKERS};
}