import * as types from './actionTypes';
import SimulationApi from '../api/simulationApi';
import * as loadingActions from './loadingActions';

export function getSimulationSuccess(simulation) {
  return {type: types.LOAD_SIMULATION_SUCCESS, simulation: simulation};
}

export function getSimulation(markers, time) {
  return function (dispatch, getState) {
    dispatch(loadingActions.startLoading());
    return SimulationApi
      .getSimulation(markers, time,  simulationMarkers => {
        dispatch(loadingActions.loadFinished());
        dispatch(getSimulationSuccess(simulationMarkers));
      });
  };
}