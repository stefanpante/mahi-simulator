import {combineReducers} from 'redux';
import markers from './markerReducer';
import stage from './stageReducer';
import departureTime from './departureTimeReducer';
import simulation from './simulationReducer';
import loading from './loadingReducer';

const RootReducer = combineReducers({
  markers: markers,
  stage: stage,
  departureTime: departureTime,
  simulation: simulation,
  loading: loading
});

export default RootReducer;