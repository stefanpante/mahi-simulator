import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function markersReducer(state = initialState.markers, action) {
  switch (action.type) {
    case types.ADD_MARKER:
      return [
        ...state, Object.assign({}, action.marker)
      ];

    case types.DELETE_MARKER:
      return [...state.filter((marker, index) => index !== action.markerIndex)];

    case types.RESET:
      return [];
    default:
      return state;
  }
}