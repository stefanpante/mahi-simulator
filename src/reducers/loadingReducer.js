import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function loadReducer(state = initialState.loading, action) {
  switch (action.type) {
    case types.START_LOAD:
      return true;
    case types.LOAD_FINISHED:
      return false;
    case types.RESET:
      return false;
    default:
      return state;
  }
}