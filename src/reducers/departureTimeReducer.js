import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function departureTimeReducer(state = initialState.departureTime, action) {
  switch(action.type) {
    case types.SET_DEPARTURE_TIME:
      return action.time;
    case types.RESET:
      return 0;
    default:
      return state;
  }
}