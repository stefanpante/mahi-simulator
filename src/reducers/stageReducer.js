import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function stageReducer(state = initialState.stage, action) {
  switch(action.type) {
    case types.TRANSITION_STAGE:
      return (state + 1) % types.NUM_OF_STAGES;
    case types.RESET:
      return 0;
    default:
      return state;
  }
}