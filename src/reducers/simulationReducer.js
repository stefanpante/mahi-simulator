import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function simulationReducer(state = initialState.simulation, action) {
  switch (action.type) {
    case types.LOAD_SIMULATION_SUCCESS:
      return action.simulation;

    case types.RESET:
      return [];
    default:
      return state;
  }
}