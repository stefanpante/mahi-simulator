# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 23:04:38 2016

@author: u0079067
"""

from GeoBasics import *
import pandas as pd
import numpy as np
import os
from math import *
import SystemState

numPoints = np.arange(4)
col = ('lat','lon')
Waypoints = pd.DataFrame(index = numPoints,columns = col)

#input waypoints
Waypoints.loc[0]['lat'] = 46.494824
Waypoints.loc[0]['lon'] = -1.813784
Waypoints.loc[1]['lat'] = 46.363323
Waypoints.loc[1]['lon'] = -25.129087
Waypoints.loc[2]['lat'] = 39.905092
Waypoints.loc[2]['lon'] = -26.201646
Waypoints.loc[3]['lat'] = 25.162522
Waypoints.loc[3]['lon'] = -76.183231

speed = 3 #kn
speed = speed*1.852 #km/h

#Calculating duration of trip
duration = 0 #h
tot_dist = 0 #km
for ii in range(numPoints.size-1):
    tot_dist = tot_dist + Distance((radians(Waypoints.loc[ii]['lat']),radians(Waypoints.loc[ii]['lon'])),(radians(Waypoints.loc[ii+1]['lat']),radians(Waypoints.loc[ii+1]['lon'])))
    duration = duration + Distance((radians(Waypoints.loc[ii]['lat']),radians(Waypoints.loc[ii]['lon'])),(radians(Waypoints.loc[ii+1]['lat']),radians(Waypoints.loc[ii+1]['lon'])))/speed

## Parameter definition
BatteryCap = 4000*3600 #Joules
Ts = 60 #sec
Done = False

# Initiate States list
States = []
State0 = SystemState.SystemState(Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon'],0,0,Bearing((Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon']),(Waypoints.loc[1]['lat'], Waypoints.loc[1]['lon'])),BatteryCap)
States.append(State0)

while not Done:
        
    