# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 12:11:41 2016

@author: u0079067
"""

class SystemState:
    def __init__(self, lat=float('NaN'), lon=float('NaN'), Vnorth=float('NaN'), Veast=float('NaN'), Head=float('NaN'), Sbat=float('NaN'), WaterVnorth=float('NaN'), WaterVeast=float('NaN')):
        self.lat=lat
        self.lon = lon
        self.Vnorth=Vnorth
        self.Veast=Veast
        self.Head = Head
        self.Sbat= Sbat
        self.WaterVnorth = WaterVnorth
        self.WaterVeast = WaterVeast