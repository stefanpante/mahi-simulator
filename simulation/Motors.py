# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 13:41:56 2016

@author: u0079067
"""

class Motor( object ):
    def GetMaxPower(self):
        raise NotImplementedError( "Should have implemented this" )
    
    def GetMechanicalPower( self, power ):
        raise NotImplementedError( "Should have implemented this" )
        
        
class TorqueedoMotorUltraLight( Motor ):
    def GetMaxPower(self):
        return 400
        
    def GetMechanicalPower(self, power):
        return power*0.45;
        
class TorqueedoMotorCruise2( Motor ):
    def GetMaxPower(self):
        return 2000
        
    def GetMechanicalPower(self, power):
        return power*0.45;