# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 11:32:48 2016

@author: u0079067
"""

from pysolar import solar
from pysolar import radiation
import datetime
import numpy as np
from GeoBasics import *
import pandas as pd
import numpy as np
import os
import sys
from math import *
import SystemState
import boat
import Motors

timeString = sys.argv[2]
timepieces = timeString.split('/')

year = int(timepieces[2])
month = int(timepieces[1])
day = int(timepieces[0])

T = datetime.datetime(2016,1,1,0,0,0)
TSeries = [T]
for tt in range(365):
    T += datetime.timedelta(days = 1)
    TSeries.append(T)

indexes = np.arange(1,367,1)
sineseries = np.zeros(366)
for tt in range(366):
    sineseries[tt] = sin((indexes[tt]-81)*2*pi/366)*0.3+0.7

load = pd.DataFrame(index = TSeries, columns = ['CLOUDS'])
load.loc[:]['CLOUDS']=sineseries

# commandline input
coordinates = str(sys.argv[1])
coordinatePairs = coordinates.split(':')
latLongs = []

for pair in coordinatePairs:
    latLon = pair.split(',')
    latLongs.append((float(latLon[0]), float(latLon[1])))

numOfCoordinates =  len(latLongs)
numPoints = np.arange(numOfCoordinates)
col = ('lat','lon')
Waypoints = pd.DataFrame(index = numPoints,columns = col)

for index in range(0, numOfCoordinates):
    lat, lon = latLongs[index]
    Waypoints.loc[index]['lat'] = lat
    Waypoints.loc[index]['lon'] = lon

BatteryCap = 2700*3600 #Joules
Ts = 30*60 #sec
boatDyn = boat.BoatDynamics()
motor=Motors.TorqueedoMotorCruise2()
bootje=boat.BoatSpecs(BatteryCap,motor, boatDyn)
controller=boat.ControllerBoat([], bootje)

panelSurfaceArea=3
panelEfficiency=0.2
clouds = 0.2 #1 is totally clouded
wayPointNum = 0

States = []
currentState = SystemState.SystemState(Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon'],0,0,Bearing((Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon']),(Waypoints.loc[1]['lat'], Waypoints.loc[1]['lon'])),BatteryCap, 0, 0)
Done = False
d = datetime.datetime(year, month, day,8,0,0)

waypointIndex = 1

while waypointIndex<Waypoints.size/2:
    nextWayPoint = Waypoints.loc[waypointIndex]

    States.append(currentState)
    d += datetime.timedelta(seconds=Ts)
    dc = datetime.datetime(2016,d.month,d.day,0,0,0)
    clouds = load.loc[dc].CLOUDS
    altitude_deg = solar.get_altitude_fast(currentState.lat, currentState.lon, d)
#    azimuth_deg = solar.GetAzimuth(currentState.lat, currentState.lon, d)
    if altitude_deg > 0:
        influx = radiation.get_radiation_direct(d, altitude_deg)*(1-clouds)*panelEfficiency*panelSurfaceArea
    else:
        influx = 0
    bearing, motorPower = controller.MakeDecision(influx, currentState, nextWayPoint,Ts)
    currentState = controller.UpdateState(influx, currentState, motorPower,bearing,Ts)
    currentState.WaterVnorth = 0
    currentState.WaterVeast = 0
    distToWp = Distance((currentState.lat, currentState.lon), (nextWayPoint['lat'], nextWayPoint['lon']))
#    print "State",d,currentState.lat, currentState.lon, influx, currentState.Sbat, int(distToWp/1000), waypointIndex
    if distToWp < 50000:
        waypointIndex+=1

for state in States:
    print str(state.lat) + "," + str(state.lon)

