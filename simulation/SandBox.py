# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 11:32:48 2016

@author: u0079067
"""

#import solar
#import radiation
import datetime
import numpy as np
import SystemState
from GeoBasics import *
import pandas as pd
import numpy as np
import os
from math import *
import SystemState

numPoints = np.arange(4)
col = ('lat','lon')
Waypoints = pd.DataFrame(index = numPoints,columns = col)

#input waypoints
Waypoints.loc[0]['lat'] = 46.494824
Waypoints.loc[0]['lon'] = -1.813784
Waypoints.loc[1]['lat'] = 46.363323
Waypoints.loc[1]['lon'] = -25.129087
Waypoints.loc[2]['lat'] = 39.905092
Waypoints.loc[2]['lon'] = -26.201646
Waypoints.loc[3]['lat'] = 25.162522
Waypoints.loc[3]['lon'] = -76.183231



#latitude_deg = 50.864896 # positive in the northern hemisphere
#longitude_deg = 4.706 # negative reckoning west from prime meridian in Greenwich, England
###d = datetime.datetime.now()
##altitude_deg = solar.GetAltitude(latitude_deg, longitude_deg, d)
##azimuth_deg = solar.GetAzimuth(latitude_deg, longitude_deg, d)
##r = radiation.GetRadiationDirect(d, altitude_deg)
##print r
#
#hours = np.arange(0,24,1)
#summer  = np.zeros(24)
#winter  = np.zeros(24)
#
#for ii in hours:
#    d = datetime.datetime(2016,1,1,ii,0,0)
#    altitude_deg = solar.GetAltitude(latitude_deg, longitude_deg, d)
#    winter[ii] = radiation.GetRadiationDirect(d, altitude_deg)
#    d = datetime.datetime(2016,7,1,ii,0,0)
#    altitude_deg = solar.GetAltitude(latitude_deg, longitude_deg, d)
#    summer[ii] = radiation.GetRadiationDirect(d, altitude_deg)
#
#plt.plot(summer,'r')
#plt.plot(winter,'b')

import boat
import Motors

BatteryCap = 4000*3600 #Joules
Ts = 60 #sec

boatDyn = boat.BoatDynamics()
motor=Motors.TorqueedoMotorUltraLight()
bootje=boat.BoatSpecs(100,motor, boatDyn)
controller=boat.ControllerBoat([], bootje)
#print controller.SolveVelocity(0,1/0.45,0,2)
States = []
State0 = SystemState.SystemState(Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon'],0,0,Bearing((Waypoints.loc[0]['lat'], Waypoints.loc[0]['lon']),(Waypoints.loc[1]['lat'], Waypoints.loc[1]['lon'])),BatteryCap, 0, 0)
States.append(State0)
newBearing = Bearing((State0.lat, State0.lon),(Waypoints.loc[1]['lat'], Waypoints.loc[1]['lon']))
State1 = controller.UpdateState(State0,motor.GetMaxPower()/10,newBearing, Ts)
#print State1.lat
#print State1.lon
#print State1.Vnorth
#print State1.Veast
#print State1.Head
#print State1.Sbat
#print State1.WaterVnorth
#print State1.WaterVeast