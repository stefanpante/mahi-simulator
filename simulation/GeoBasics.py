# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 20:49:01 2016

@author: u0079067
"""
    
def NextWaypoint(Startpoint, Speed, Bearing, Time):
    #Startpoint coordinates should be tuples in radians, Speed in m/s and Bearing in radians, Time in s
    #Return waypoint is a tuple in radians
    from math import atan2, asin, sin, cos, radians, degrees
    lat0 = radians(Startpoint[0])
    lon0 = radians(Startpoint[1])
    d = Speed*Time/1000 #km
    R = 6372.7954 #km
    lat1 = asin(sin(lat0)*cos(d/R)+cos(lat0)*sin(d/R)*cos(Bearing)) 
    lon1 = lon0+atan2(sin(Bearing)*sin(d/R)*cos(lat0),cos(d/R)-sin(lat0)*sin(lat1)) 
    return (degrees(lat1),degrees(lon1))    
    
def Bearing(Startpoint, Endpoint):
    #Input coordinates should be tuples in radians, return radians is in degrees
    from math import atan2, cos, sin, degrees, radians
    lat0 = radians(Startpoint[0])
    lon0 = radians(Startpoint[1])
    lat1 = radians(Endpoint[0])
    lon1 = radians(Endpoint[1])
    bearing =  atan2(sin(lon1-lon0)*cos(lat1),cos(lat0)*sin(lat1)-sin(lat0)*cos(lat1)*cos(lon1-lon0))
    return bearing

def Radiansext(degrees, minutes, seconds):
    #input as foat, returns float
    import numpy as np
    return (degrees+minutes/60+seconds/3600)*np.pi/180
    
def Distance(Startpoint, Endpoint):
    #Input in radians, output in m
    from math import radians, cos, sin, asin, sqrt
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    lat1 = radians(Startpoint[0])
    lon1 = radians(Startpoint[1])
    lat2 = radians(Endpoint[0])
    lon2 = radians(Endpoint[1])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6372.7954 # Radius of earth in kilometers. Use 3956 for miles
    return c * r *1000