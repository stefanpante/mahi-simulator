# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 13:31:35 2016

@author: u0079067
"""
import SystemState
from math import cos, pi, sin
import numpy as np
import GeoBasics

class BoatSpecs:
    def __init__(self, batteryCap, motorType, boatDynamics):
        self.batteryCap = batteryCap
        self.motorType = motorType
        self.boatDynamics = boatDynamics
              
class BoatDynamics:
    v = [0,0.257,0.514,0.772,1.029,1.286,1.543,1.801,2.058] #m/s
    R_total = [0,0.2,0.6,1.3,3.2,8.6,17.9,43.9,62.3] #N    
    
    def getDrag(self, Vboat):
        return np.interp(Vboat, self.v, self.R_total)
        
    def getMaxVelocity(self):
        return self.v[-1]
        
    def getMass(self):
        return 60 #kg

    
class ControllerBoat:
    import GeoBasics
    def __init__(self, wayPoints, boatSpecs):
        self.wayPoints = wayPoints
        self.boatSpecs = boatSpecs
        
    def SolveVelocity(self, v0, motorPower, motorAngle):
        dt = 0.05 #sec
        if motorPower == 0:
            return 0      
        deltaAmax = 0.005 #m/s²
        v1 = v0
        v0 = np.Inf
        while abs(v1-v0)/dt>deltaAmax:
            v0 = max(v1,0.001)
            v1 = (-self.boatSpecs.boatDynamics.getDrag(v0)+ \
                (cos(motorAngle)*self.boatSpecs.motorType.GetMechanicalPower(motorPower))/v0 + \
                self.boatSpecs.boatDynamics.getMass()*v0*2/dt)*dt/(2*self.boatSpecs.boatDynamics.getMass())    
        if v1<0 or v1 >self.boatSpecs.boatDynamics.getMaxVelocity():
            raise ValueError('Vboat out of range', v1)
        return float(v1)
        
    def UpdateState(self, influx, currentState, motorPower,newBearing, Ts):
        import math
        Vnorth = currentState.Vnorth
        Veast = currentState.Veast
        Head = currentState.Head
        HeadMat = np.matrix([[cos(Head), -sin(Head)],[sin(Head), cos(Head)]])
        Vboat_water_north = Vnorth-currentState.WaterVnorth
        Vboat_water_east = Veast-currentState.WaterVeast
        Vboat_water_earth = np.matrix([[Vboat_water_north],[Vboat_water_east]])
        Vboat_water_local = HeadMat*Vboat_water_earth 
        new_Vboat_water_local = np.matrix([[self.SolveVelocity(Vboat_water_local[0], motorPower, 0)],[0]])
        newHeadMat = np.transpose(np.matrix([[cos(newBearing), -sin(newBearing)],[sin(newBearing), cos(newBearing)]]))
        newVboat_water_earth = newHeadMat*new_Vboat_water_local 

        newState = SystemState.SystemState()

        newState.Vnorth = newVboat_water_earth[0,0]+currentState.WaterVnorth
        newState.Veast = newVboat_water_earth[1,0]+currentState.WaterVeast
#        print "water",newState.Vnorth,newState.Veast
#        print math.atan2(newState.Veast, newState.Vnorth)
        newWaypoint = GeoBasics.NextWaypoint((currentState.lat,currentState.lon),np.linalg.norm([newState.Vnorth,newState.Veast]), -math.atan2(newState.Veast, newState.Vnorth), Ts)
        newState.lat = newWaypoint[0]       
        newState.lon = newWaypoint[1]
        
        deltaE = (influx - motorPower)*Ts
        if deltaE > 0:
            newState.Sbat = min(currentState.Sbat + deltaE*0.75*0.9, self.boatSpecs.batteryCap)
        else:
            newState.Sbat = currentState.Sbat + deltaE
        if newState.Sbat<-0.1:
            raise ValueError('Battery discharged')
        newState.Head = newBearing
        return newState

    def MakeDecision(self, influx, currentState, nextWayPoint, Ts):
        import GeoBasics
        newBearing = GeoBasics.Bearing((currentState.lat, currentState.lon), (nextWayPoint['lat'], nextWayPoint['lon']))
        motorPower = max(min(currentState.Sbat/Ts, 280),0)
#        print motorPower
        
        return newBearing, motorPower
