import webpack from 'webpack';
import webpackConfig from '../webpack.config.prod';
import colors from 'colors';

process.env.NODE_ENV = 'production';
console.log('Generating production build');

webpack(webpackConfig).run((err, stats) => {
  if(err){
    console.log(err.bold.red);
    return 1;
  }

  const jsonStates = stats.toJson();

  if(jsonStates.hasErrors){
    return jsonStates.errors.forEach(error => console.log(error.red));
  }

  if(jsonStates.hasWarnings){
    console.log('Webpack generated following warnings'.bold.yellow)
    return jsonStates.warnings.forEach(error => console.log(error.yellow));
  }

  console.log('compiled and written to dist');

  return 0;
});