'use strict';
const spawn = require('child_process').spawn;
const processName = 'python';
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
app.use(cors());
app.use(bodyParser.json());

function getCoordinatesFunction(res) {
  return function (coordinates, time) {
    let dat = '';
    const python = spawn(processName, ['./simulation/Environment.py', coordinates, time]);
    python.stdout.on('data', (data) => {
      dat += data.toString();
    });

    python.on('close', () => {
      const listOfLatLngs = dat.split('\n');
      const latLngs = listOfLatLngs.map((latlgn) => {
        const coords = latlgn.split(',');
        return [ parseFloat(coords[0]), parseFloat(coords[1])];
      });

      res.json(latLngs);
    });

  };
}
app.post('/coordinates', (req, res) => {
  if (!Array.isArray(req.body.markers)) {
    return res.sendStatus(400);
  }

  const latLongs = req.body.markers.map((latlong) => latlong.join(','));
  const coordinates = latLongs.join(':');
  return getCoordinatesFunction(res)(coordinates, req.body.departureTime);
});

app.listen(9021);